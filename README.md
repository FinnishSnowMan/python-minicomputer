## IN ORDER TO RUN THIS PROGRAM, YOU MUST INSTALL VERSION (3.11.5) OF PYTHON!

-- python installation guide: --

1. Download Python: Visit the official Python website (https://www.python.org/downloads/) and download the latest version of Python for your operating system (Windows, macOS, or Linux).

2. Run the Installer: Run the installer you downloaded, and follow the installation instructions. During the installation process, you may have the option to add Python to your system's PATH, which is recommended as it allows you to run Python from the command line without specifying the full path to the Python executable.

3. Verify Installation: After installation, open your command prompt or terminal and run the following command to verify that Python is installed correctly: python --version

this command should show you what version of Python you've installed.

Once Python is installed, you can create and run Python scripts (.py files) by opening a command prompt or terminal, navigating to the directory where your Python script is located, and running the script using the 'python' command followed by the script's filename.

## for program installation, please refer to the 'Installation Guide' file in the repository.
